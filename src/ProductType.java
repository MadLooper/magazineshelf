/**
 * Created by KW on 8/07/2017.
 */
public enum ProductType {
    FOOD, INDUSTRIAL, ALCOHOL
}
